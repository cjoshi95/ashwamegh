<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/

$db = new DbConnect();
$msg = "";

?>


<!DOCTYPE HTML>
<html>
<head>
    <title>Order Data - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
          crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
        src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
          crossorigin="anonymous">


</head>
<body>
<?php include 'inc/header.php'; ?>

<div class="container mt-5">
    <div class="row">

        <div class="col-md-4">
            <?php

            if ($_SERVER['REQUEST_METHOD'] == "GET")
            {
                if (isset($_GET['batteryid']))
                {

                    $orderid = $_GET['batteryid'];
                    $sql = "select * from batteries where srno=$orderid";
                    $quer = mysqli_query($db->getDb(), $sql);
                    $row = mysqli_fetch_row($quer);
                    echo "<h2 class='text-center'>" . $row[1] . "'s Information Update</h2>";

                    echo "<form method=\"post\">";
                 ?>

                    Battery ID:
                    <input class="form-control" value="<?php echo $row[0] ?>" disabled><br>
                    Battery Name :
                    <input class="form-control" value="<?php echo $row[1]; ?>" disabled><br>
                    Description :
                    <input class="form-control" value="<?php echo $row[2]; ?>" disabled><br>
                    Image URL :
                    <input class="form-control" value="<?php echo $row[3]; ?>"><br>
                    Cost :
                    <input class="form-control" value="<?php echo $row[4]; ?>"><br>
                    Brand :
                    <input class="form-control" value="<?php echo $row[5]; ?>" disabled><br>
                    Type :
                    <input class="form-control" value="<?php echo nameFromType($row[6]); ?>" disabled><br>
                    Quantity :
                    <input class="form-control" value="<?php echo $row[7]; ?>"><br>

                    <button type="submit" class="btn btn-primary">Update</button>
                    <?php
                    echo "</form>";
                }
            }
            ?>

        </div>
    </div>
</div>
</body>
</html>
