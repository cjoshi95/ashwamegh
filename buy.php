<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
$db = new DbConnect();
?>


<!DOCTYPE HTML>
<html>
<head>
    <title>Buy Battery - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body>
<?php include 'inc/header.php'; ?>

<div class="container mt-5">

    <?php


    if(!is_loggedIn())
    {
        echo "<div class='container mt-5'><h2 class='text-center'>You need to Login first.. <a href='login.php'>Click here</a> to Login now or <a href='register.php'>Click here</a> for Register</h2></div>";
//    header("Location: http://$_SERVER[SERVER_NAME]");
    }
    else if ($_SERVER['REQUEST_METHOD'] == "POST")
    {
        if (isset($_POST['name']) && isset($_POST['address']) && isset($_POST['pincode']) && isset($_GET['product']))
        {
            $name = $_POST['name'];
            $address = $_POST['address'];
            $pincode = $_POST['pincode'];
            $product = $_GET['product'];

            $sql2 = "select * from batteries where srno=" . $product;
            $quer2 = mysqli_query($db->getDb(), $sql2);
            $row = mysqli_fetch_row($quer2);


            $sql = "insert ignore into orders (batteryid,userid,fullname,address,pincode,amount) VALUES($product,$_SESSION[usersrno],'$name','$address',$pincode,$row[4])";
            $quer = mysqli_query($db->getDb(),$sql);

            if($quer)
            {
                $sql1 = "update batteries set quantity = quantity-1 where srno=$product";
                $quer1 = mysqli_query($db->getDb(),$sql1);

                ?>
                <div class="border border-dark p-5">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="auto border border-primary d-flex justify-content-around">
                                <img src="<?php echo $row[3]; ?>" class="buynow-logo">
                            </div>

                            <div class="mt-5">
                                <span class="price"><b>Price :</b>  Rs. <?php echo $row[4]; ?></span>
                            </div>
                            <div class="mt-2">
                            </div>

                        </div>
                        <div class="col-md-8 text-right">
                            <h1 class="ordersuccess p-2 bg-success">Order Placed Successfully...</h1>
                            <h1 class="display-4 text-right"><?php echo $row[1]; ?></h1>
                            <h3 class="desc"><b>Brand : </b> <?php echo $row[5]; ?></h3>
                            <h3 class="desc"><b>Type : </b> <?php echo nameFromType($row[6]); ?></h3>
                            <h3 class="desc"><b>Vehicle Brand : </b> <?php echo $row[9]; ?></h3>
                            <h3 class="desc"><b>Availability : </b> <?php echo $row[7] . " in Stock."; ?></h3>
                        </div>
                        <div class="col-md-12">
                            <span class="desc"><b>Name : </b> <?php echo $name; ?></span><br>
                            <span class="desc"><b>Address : </b> <?php echo $address; ?></span><br>
                            <span class="desc"><b>Pincode : </b> <?php echo $pincode; ?></span>

                        </div>
                    </div>
                </div>
                <?php
            }
            else
            {

            }
        }
    }
    else if ($_SERVER['REQUEST_METHOD'] == "GET")
    {
        if (isset($_GET['product']))
        {
            $product = $_GET['product'];
            $sql = "select * from batteries where srno=" . $product;
            $quer = mysqli_query($db->getDb(), $sql);
            $row = mysqli_fetch_row($quer);
            ?>
            <div class="border border-dark p-5">
                <div class="row">
                    <div class="col-md-4">
                        <div class="auto border border-primary d-flex justify-content-around">
                            <img src="<?php echo $row[3]; ?>" class="buynow-logo">
                        </div>

                        <div class="mt-5">
                            <span class="price"><b>Price :</b>  Rs. <?php echo $row[4]; ?></span>
                        </div>
                        <div class="mt-2">
                        </div>

                    </div>
                    <div class="col-md-8 text-right">
                        <h1 class="display-4 text-right"><?php echo $row[1]; ?></h1>
                        <h3 class="desc"><b>Brand : </b> <?php echo $row[5]; ?></h3>
                        <h3 class="desc"><b>Type : </b> <?php echo nameFromType($row[6]); ?></h3>
                        <h3 class="desc"><b>Vehicle Brand : </b> <?php echo $row[9]; ?></h3>
                        <h3 class="desc"><b>Availability : </b> <?php echo $row[7] . " in Stock."; ?></h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mt-5 mb-5">
                    <form method="post">
                        <b>Full Name &nbsp;&nbsp;</b><input name="name" type="text" class="form-control"><br>
                        <b>Address &nbsp;&nbsp;</b><textarea name="address" class="form-control"></textarea><br>
                        <b>Pincode &nbsp;&nbsp;</b><input name="pincode" type="number" class="form-control"><br>
                        <button class="btn btn-secondary" type="submit" name="submit">Order Now</button>
                    </form>
                </div>
                <div class="col-md-6 mt-5 mb-5 border border-success">
                    <h2>Payment Method</h2>
                    <input type="radio" name="payment" id="payment">  Cash on Delivery<br>
                    <input type="radio" name="payment" id="payment" disabled>  PayTM
                </div>
            </div>
            <?php
        }
    }
    ?>

</div>

</body>
</html>