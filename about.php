<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/

$db = new DbConnect();

?>

<!DOCTYPE HTML>
<html>
<head>
    <title>About Us - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body class="aboutbg">
<?php include 'inc/header.php'; ?>
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <img src="img/logo.png" alt="">
        </div>
    </div>

    <p>    <span class="abouttext">
        Started in 2012, Ashwamegh Enterprises is India’s first online multi-brand battery marketplace, having a complete range of 100% genuine batteries from brands like SF Sonic, Exide, Okaya, MtekPower, Su-Kam, DigiPower, Tata Green and AC Delco. Our aim is to lead the online market of offering multi-branded batteries across the major cities in India.
  </span>
    </p>
    <p>    <span class="abouttext">
        At present, we have services in over 100 cities across India. To facilitate our customers in choosing the right battery for their cars and inverters, we make it easy to find important details like warranty, price, technical specifications and features. We focus to help our customer save time and money. We offer extremely competitive prices, unmatchable and absolutely FREE delivery and installation service.
              </span>
    </p>
    <p>    <span class="abouttext">
        In a short span of time BatteryBhai has emerged as the ultimate source for buying Car/Inverter batteries, Inverters & Home UPS online.
              </span>
    </p>


    <div class="delivery_row mb-5">
        <div class="wrap">
            <div class="col_1_of_2 span_1_of">
                <div class="icon_home"><img src="http://www.batterybhai.com/common/images/genuin_icon.png" alt=""></div>
                <div class="text_home">
                    <span>100% GENUINE PRODUCTS</span>
                    <em>Brand new and 100% genuine products from trusted sellers only</em>
                </div>
            </div>
            <div class="col_1_of_2 span_1_of">
                <div class="icon_home"><img src="http://www.batterybhai.com/common/images/best_price_icon.png" alt="">
                </div>
                <div class="text_home">
                    <span>BEST PRICES</span>
                    <em>Get big discounts &amp; maximum exchange value </em>
                </div>
            </div>
            <div class="col_1_of_2 span_1_of">
                <div class="icon_home"><img src="http://www.batterybhai.com/common/images/delivery.png" alt=""></div>
                <div class="text_home">
                    <span>FREE DELIVERY/INSTALLATION</span>
                    <em>Get free professional installation in just few hours</em>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>