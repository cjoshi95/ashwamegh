<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/

$db = new DbConnect();
$err = "";

if ($_SERVER['REQUEST_METHOD'] == "POST")
{
    if (isset($_POST['batname']))
    {
        $batname = $_POST['batname'];
        $batdesc = $_POST['batdesc'];
        $batimg = $_POST['batimg'];
        $batcost = $_POST['batcost'];
        $batbrand = $_POST['batbrand'];
        $batquant = $_POST['batquant'];
        $batdetaild = $_POST['batdetail'];
        $battype = $_POST['battype'];
        $batvehbrand = $_POST['batvehbrand'];

        $sql = "insert into batteries (title,description,img,cost,brand,type,quantity,detailed_description,vehiclebrand) VALUES ('$batname','$batdesc','$batimg',$batcost,'$batbrand','$battype',$batquant,'$batdetaild','$batvehbrand')";
        $quer = mysqli_query($db->getDb(), $sql);


        if ($quer)
        {
            $err .= "<p class=\"text-center bg-success p-4\">Successfully Inserted Battery Data.</p>";
        } else
        {
            $err .= "<p class=\"text-center bg-danger p-4\">Something went wrong. Please try again.</p>";
        }
    }
}

?>

<!DOCTYPE HTML>
<html>
<head>
    <title>Distributor Panel - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body>
<?php include 'inc/header.php'; ?>

<div class="container mt-5">
    <div class="jumbotron">
        <h1 class="display-4">Distributor Panel</h1>
        <i class="fas fa-shopping-cart"></i>

        <p class="lead">Distributors Panel to View/Edit/Remove the Orders or Inventory.</p>
        <hr class="my-4">
    </div>
    <?php echo $err; ?>
    <div class="row">
        <div class="col-md-12">
            <h3>Add new Battery</h3>
            <form class="form-inline mb-5" method="post">

                <input placeholder="Battery Name" type="text" name="batname" class="form-control m-1"/><br>

                <input placeholder="Battery Description" type="text" name="batdesc" class="form-control m-1"/><br>

                <input placeholder="Image URL" type="text" name="batimg" class="form-control m-1"/>

                <input placeholder="Cost" type="number" name="batcost" class="form-control m-1"/><br>

                <label for="batbrand">Brand
                </label>
                <select class="form-control m-1" name="batbrand" id="batbrand">
                    <option value="Exide">Exide</option>
                    <option value="SFSonic">SF Sonic</option>
                </select> <br>


                <input placeholder="Quantity" type="number" name="batquant" class="form-control m-1"/><br>

                <textarea placeholder="Battery Detailed Description" name="batdetail"
                          class="form-control m-1"></textarea><br>

                <label for="battype">Battery Type
                </label>
                <select class="form-control m-1" name="battype" id="battype">
                    <option value="4">Four Wheeler</option>
                    <option value="3">Three Wheeler</option>
                    <option value="2">Two Wheeler</option>
                    <option value="I">Inverter</option>
                    <option value="S">Solar Panels</option>
                    <option value="U">UPS Battery</option>
                </select> <br>

                <label for="batvehbrand">Vehicle Brand
                </label>
                <select class="form-control m-1" name="batvehbrand" id="batvehbrand">
                    <option value="TVS">TVS</option>
                    <option value="honda">Honda</option>
                    <option value="hero">Hero</option>
                </select><br>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">User's Order History</h1>

            <form method="get" class="form-inline">
                Select status :&nbsp;
                <select class="form-control" name="status" id="status">
                    <option value="processing">Processing</option>
                    <option value="dispatched">Dispatched</option>
                    <option value="completed">Completed</option>
                </select> &nbsp;&nbsp;&nbsp;

                Select Date : (Leave Blank for all time data)&nbsp;
                <input class="form-control" type="date" name="orderdate"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input class="btn btn-primary" type="submit" name="submit" id="submit" value="Search"/>
            </form> <br><br>
            <table class="table">
                <thead>
                <th>Srno</th>
                <th>Battery name</th>
                <th>User Email</th>
                <th>Full Name</th>
                <th>Address</th>
                <th>pincode</th>
                <th>Order Time</th>
                <th>Amount</th>
                <th>Order Status</th>
                </thead>
                <tbody>
                <?php

                $sql = "select * from orders";
                if(isset($_GET['status']))
                    $sql .= " where orderstatus='$_GET[status]'";
                if(isset($_GET['orderdate']) && $_GET['orderdate']!="" && isset($_GET['status']) && $_GET['status']!="")
                    $sql .= " and ordertime='$_GET[orderdate]'";
                else if(isset($_GET['orderdate']) && $_GET['orderdate']!="" && isset($_GET['status']))
                    $sql .= " where ordertime = '$_GET[orderdate]'";

                $quer = mysqli_query($db->getDb(), $sql);
                while ($row = mysqli_fetch_row($quer))
                {
                    $sql1 = "select title from batteries where srno=$row[1]";
                    $quer1 = mysqli_query($db->getDb(), $sql1);
                    $batrow = mysqli_fetch_row($quer1);

                    $sql2 = "select username from accounts where srno=$row[2]";
                    $quer2 = mysqli_query($db->getDb(), $sql2);
                    $userrow = mysqli_fetch_row($quer2);
                    ?>

                    <tr>
                        <td><a href="orderdata.php?orderid=<?php echo $row[0] ?>"><?php echo $row[0] ?></a></td>
                        <td><?php echo $batrow[0]; ?></td>
                        <td><?php echo $userrow[0]; ?></td>
                        <td><?php echo $row[3]; ?></td>
                        <td><?php echo $row[4]; ?></td>
                        <td><?php echo $row[5]; ?></td>
                        <td><?php echo $row[6]; ?></td>
                        <td><?php echo $row[7]; ?></td>
                        <td><?php echo $row[8]; ?></td>
                    </tr>

                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Battery Inventory Data</h1>
            <table class="table">
                <thead>
                <th>Srno</th>
                <th>Battery name</th>
                <th>Description</th>
                <th>Image</th>
                <th>Cost</th>
                <th>Brand</th>
                <th>Type</th>
                <th>Quantity</th>
                <th>Vehicle Brand</th>
                </thead>
                <tbody>
                <?php
                $sql = "select * from batteries";
                $quer = mysqli_query($db->getDb(), $sql);
                while ($row = mysqli_fetch_row($quer))
                {
                    if ($row[7] == 0)
                        echo "<tr class='table-danger'>";
                    else
                        echo "<tr>";
                    ?>

                    <td><a href="batterydata.php?batteryid=<?php echo $row[0] ?>"><?php echo $row[0] ?></a></td>
                    <td><?php echo $row[1]; ?></td>
                    <td><?php echo $row[2]; ?></td>
                    <td><img src="<?php echo $row[3]; ?>" alt="" class="thumbnail iconsize"></td>
                    <td><?php echo $row[4]; ?></td>
                    <td><?php echo $row[5]; ?></td>
                    <td><?php echo nameFromType($row[6]); ?></td>
                    <td><?php echo $row[7]; ?></td>
                    <td><?php echo $row[9]; ?></td>
                    </tr>

                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>