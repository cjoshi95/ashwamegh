<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/

$db = new DbConnect();


?>


<!DOCTYPE HTML>
<html>
<head>
    <title>Knowledge Base - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body>
<?php include 'inc/header.php'; ?>
<div class="container">

    <h1>How to Jump Start A Car?</h1>

    <div class="row">
        <div class="col-md-12">
            <span class="kbasetitle">What You Will Need?</span>

            <ul>
                <li>Another car, in running condition.</li>
                <li>Jumper cables</li>
                <li>You'll also need to park the running car next to the stalled car in such a way that the jumper
                    cables can reach both batteries.
                </li>
            </ul>

            <span class="kbasetitle">Things to keep in mind before jump-starting the car:</span>

            <ul>
                <li>Turn both keys (working and stalled car) to the OFF position.</li>
                <li>Make sure both batteries have the same required voltage (e.g. 12V)</li>
                <li>Turn off lights, A.C and other electrical loads</li>
                <li>No electrical parts of the two cars should touch each other</li>
                <li>Gear of the vehicle should be in neutral position</li>
                <li>Your vehicle should have same earthed terminal, otherwise, you should follow the instruction
                    provided by the manufacturer and
                </li>
                <li>Make sure that cables are not ragged or damaged</li>
            </ul>

            <span class="kbasetitle">Once you have taken care of the above points, you should follow the below steps in sequence:</span>

            <ul>
                <li>On negative grounded system, you need to connect the positive terminal of the battery of your stalled car
                    with the positive terminal of jump-starting battery</li>
                <li>Then you are advised to connect one end of the other cable to the negative terminal (-) of the jump-starting
                    battery</li>
                <li>Now connect the second end of the other cable to a section of unpainted metal on the dead car. This can be
                    the engine block or car frame of the vehicle that will be started.</li>
                <li>Ensure both cables are away from any moving parts of the vehicle (e.g. fan)</li>
                <li>First, start the car with the good battery, and leave it running. If the battery in the stalled car was
                    really badly drained, it may help to leave them connected for a minute with the good car running before you
                    try to start the dead car. This will allow a little charge to build up in the dead battery.</li>
                <li>Turn the key in the dead car to start and it should start right up!</li>
                <li>If your vehicle doesn’t start within 30 seconds, you are advised to call your auto electrician.</li>
            </ul>
            <p style="text-align:center">
            <img class="center" src="http://www.batterybhai.com/common/images/jump-start-car-with-jumper-cables.jpg" alt="How to Jump Start a Car">
            </p>

            <span class="kbasetitle">Disconnecting the Jumper Cables:</span>
            <p>Start removing cables in reverse order (you have to remove the cable, which is connected to engine block or car frame first).</p>

            <span class="kbasetitle">Warning!</span>
            <p>If you own a modern vehicle with electronic management system, then you are advised not to jump-start your vehicle without "protected" jump starter leads. For such vehicles, you should follow the owner's handbook for jump-starting procedure.</p>
        </div>
    </div>

</div>
</body>
</html>