<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/

$db = new DbConnect();

?>

<!DOCTYPE HTML>
<html>
<head>
    <title>Locate Us - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
          crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body>
<?php include 'inc/header.php'; ?>

<div class="container mt-5">

    <div class="jumbotron">
        <h1 class="display-4">Locate Us</h1>
        <hr class="my-4">
        <p class="lead">Following is the list of Our service centers.</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <img src="https://www.google.com/maps/vt/data=8v8_d_qk4XmBgQClk7MvFQzR3WtYh0gjsS_dDFWAGK9OFspGp_tp1ClK5zAnQmY2gW7qluRSwQ6uRvYe8Bq6fnu50lzvohbZt4KQe44G41D4NOCBdFJFjiT2XdWn0DN9EVcU71SNzhfliNNElviYVBLarUIbCIRtsv8ldCMpkMJUkr-fwWBcrimeP3cu3njT84ztLmZGhq7ribVyIzAckawK7u6p8ESWGORRFqJyPoHxXPA-6E3qublT1LADmolQP5mUu-K7m9lXyLTzU3ryTa1INFilw3nfYW0cSsXeuCQ" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Pune</h5>
                    <p class="card-text">28, Paud Rd, Sheela Vihar Colony, Pune, Maharashtra 411038</p>
                    <a href="#" class="btn btn-primary"><i class="fas fa-phone"></i> +91 95274 22547</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <img src="https://www.google.com/maps/vt/data=9ssMknQKcblm0-RvDNKkHvfGd7cNoCV2AFkeRs4aeCBvBYNfxE4laD64UFF-vz6IeqkKmD0riyqyA5YzQM7vi-tcqtRP4-caZo_mp-GXeNrdgtKzBIGOD4GusQAuDSpcR79Chfs0DaXbqpnwLi5ezwcFZtojDNrcslvX2ly0iwNjZFqHCP_9p_zGEA-qJElXTf8bvrT53TNydFcM_lbNKlxs7OnLMaCVb0yN75K2AcxcTKqnpJQtT3xODrCTvTe4BqOskQAcgqT8an9LuT9jXvos_3nK2McNz3wQFt3YHvI" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Pimpri Chinchwad</h5>
                    <p class="card-text">Shirode Rd, Prabhat Nagar, Pimple Gurav, Pimpri-Chinchwad, Maharashtra 411027</p>
                    <a href="#" class="btn btn-primary"><i class="fas fa-phone"></i> +91 97632 03954</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <img src="https://www.google.com/maps/vt/data=KpPIrYSvhnakwABrJN0axIyDv8Poma2oMj5Fb5-Madpytu0GV9L8B8tmPY6dAUDi3647fpI6or5dAjfpi4WCRru5EpnaLDH0nVJ3-aA4rTsSjlxNYpnaVcHPe7jHNn5v45vpIfzZdvY1BdZiJL5TbU1hIviaN2CzEGM-uJ5J-nal-sB6Se8mAyuuUexZz37GaKLUviWcxoMgrQ0mn1S4bEzjWbkJ5ulYLeFBkdjv-T3RWBvdWjD-oUs0PlsLPrcdNOFi6XixWKZFnYqCf0KjPWzu_agbonr_vNhtlIkQgoo" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Mumbai</h5>
                    <p class="card-text">87-1, Yadav Nagar, Chandivali, Powai, Mumbai, Maharashtra 400072</p>
                    <a href="#" class="btn btn-primary"><i class="fas fa-phone"></i> +91 70217 91112</a>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>
