-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2019 at 05:08 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `battery`
--
CREATE DATABASE IF NOT EXISTS `battery` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `battery`;

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `srno` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `contact` int(15) NOT NULL,
  `createdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` varchar(5) NOT NULL DEFAULT 'User'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`srno`, `username`, `password`, `contact`, `createdate`, `role`) VALUES
(6, 'vj@bj.com', '11', 2147483647, '2019-04-13 15:57:05', 'User'),
(7, 'ss@ssss.com', 'ss', 2147483647, '2019-04-13 16:00:36', 'User'),
(8, 'pranavpdd@gmail.com', '11', 2147483647, '2019-04-15 14:49:50', 'Dis'),
(9, 'a@a.com', '11', 2147483647, '2019-04-16 10:45:41', 'Adm'),
(10, 'b@b.com', '11', 2147483647, '2019-04-17 08:36:15', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `batteries`
--

CREATE TABLE `batteries` (
  `srno` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `img` varchar(500) NOT NULL,
  `cost` int(10) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `quantity` int(100) NOT NULL,
  `detailed_description` varchar(5000) NOT NULL,
  `vehiclebrand` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batteries`
--

INSERT INTO `batteries` (`srno`, `title`, `description`, `img`, `cost`, `brand`, `type`, `quantity`, `detailed_description`, `vehiclebrand`) VALUES
(1, 'SF SONIC MK1440-TZ4', 'The revolutionary Gas Recombination system eliminates water loss making the battery completely maintenance free.', 'http://www.batterybhai.com/common/uploaded_files/battery_image/438007377E_1512648750_mk-1440-tz4.jpg', 1800, 'SFSonic', '4', 35, 'The revolutionary Gas Recombination system eliminates water loss making the battery completely maintenance free. The Spill Proof design guards against the possibility of leakage, even if mounted in a tilted or inclined position. The advanced Lead-Calcium technology leads to low self-discharge. Unique Flame Arrestor ensures greater safety. The AGM construction gives superior cranking power and better resistance to vibration and hence makes this battery the most reliable in the market today. The permanently sealed VRLA battery is factory-charged does not need re-filling, electrolyte or water.', 'TVS'),
(2, 'EXIDE FXL0-12XL5L-B', 'Spill-proof design guards against the possibility of leakage, even if mounted in a tilted or inclined position.', 'http://www.batterybhai.com/common/uploaded_files/battery_image/21585ECD86_1514269309_12xl5l-b_1.jpg', 2100, 'EXIDE', '4', 42, 'The revolutionary Gas Recombination system eliminates water loss making the battery completely maintenance-free. Spill-proof design guards against the possibility of leakage, even if mounted in a tilted or inclined position. Advanced Lead-Calcium technology leads to low self-discharge. Unique Flame Arrestor ensures greater safety. AGM construction gives superior cranking power and better resistance to vibration and hence makes this battery the most reliable in the market today. This permanently sealed VRLA battery comes factory-charged. It needs no refilling, electrolyte or water.', 'TVS'),
(3, 'SF SONIC SQ1440-TZ5L-B', 'The advanced Lead-Calcium technology leads to low self-discharge.', 'http://www.batterybhai.com/common/uploaded_files/battery_image/9894F8EEF3_1512653013_torque-sq1440--tz5l-b.jpg', 1750, 'SFSonic', '2', 15, 'The revolutionary Gas Recombination system eliminates water loss making the battery completely maintenance free. The Spill Proof design guards against the possibility of leakage, even if mounted in a tilted or inclined position. The advanced Lead-Calcium technology leads to low self-discharge. Unique Flame Arrestor ensures greater safety. The AGM construction gives superior cranking power and better resistance to vibration and hence makes this battery the most reliable in the market today. The permanently sealed VRLA battery is factory-charged does not need re-filling, electrolyte or water.', 'honda'),
(4, 'SF SONIC FFS0-FS1800-DIN74', 'Patented Japanese C21 alloy to withstand severe working conditions', 'http://www.batterybhai.com/common/uploaded_files/battery_image/C8E3186699_1511157925_fs1800-din74.jpg', 1750, 'SFSonic', '3', 0, 'Patented Japanese C21 alloy to withstand severe working conditions. Superior starting power, instant ignition.Arrestors to prevent acidic corrosion and sparks.Dual plate protection against shocks and vibrations.No topping up. Zero maintenance Factory charged battery.', 'honda'),
(5, 'EXIDE MTREDDIN74', 'Special paste formulation for Positive and Negative plates give higher life anticipation with flawless charge acceptance in service.', 'http://www.batterybhai.com/common/uploaded_files/battery_image/DC9D57402D_1484388432_mtreddin74.jpg', 1500, 'EXIDE', '2', 45, 'Exide MT RED, completely sealed, maintenance free batteries, come with cutting edge Ca-Ca system.  No topping up during guaranteed service life span.  Long wet shelf life, no freshening charge required for up to 6 months from manufacturing date.  Special paste formulation for Positive and Negative plates give higher life anticipation with flawless charge acceptance in service.  Double Clad Polyethylene and Glass Mat partition makes the battery highly reliable and durable. Omega type cover design makes it spill resistant.', 'hero'),
(6, 'SF SONIC FFS0-FS1440-DIN44', 'Superior starting power, instant ignition', 'http://www.batterybhai.com/common/uploaded_files/battery_image/8C7E23EB97_1511159249_73cee643c4_1446628991_flash-start-1440.jpg', 1950, 'SFSonic', '3', 22, 'Patented Japanese C21 alloy to withstand severe working conditions.  Superior starting power, instant ignition.   Arrestors to prevent acidic corrosion and sparks.  Dual plate protection against shocks and vibrations.  No topping up. Zero maintenance Factory charged battery.', 'hero'),
(7, 'EXIDE INS BRT1500', 'Easy maintenance: Float/float guide to indicate electrolyte level', 'http://www.batterybhai.com/common/uploaded_files/battery_image/E05C8727CB_1484391999_insbrt1500.jpg', 2200, 'EXIDE', 'I', 4, 'Exide Insta Brite promises superior power backup at a fantastic price. With Exide Instabrite, you can be sure that there will be no shortage of power in your home. Get instant brightness with the best. Get Exide Instabrite.  Unique features and superior technology: Advanced Hybrid Technology that is best suited to withstand high temperatures as well as thick plate construction with special paste. formulation. Special hybrid alloy system leading to low water loss and dual plate separation(PE+GM) that reduces the possibility of premature failure. Easy maintenance: Float/float guide to indicate electrolyte level. Easy handling/spill-proof: Moulded handles to ensure easy handling.Top vented lid with anti-splash guards fitted with coin flush vent plugs. Fume and leak resistant: Spark arrestor fitted in float to restrict fumes and acid during operation. Clean top with no surface leakage. Ready for commissioning: Batteries supplied in factory-charged condition.', 'hero'),
(8, 'EXIDE MILEAGE RED MREDDIN44LH', 'Magic Eye - for determination of electrolyte level and state-of-charge', 'http://www.batterybhai.com/common/uploaded_files/battery_image/FCA9D97506_1484387785_mreddin44lh.jpg', 1750, 'EXIDE', 'I', 10, 'Special side vented cover design - excellent spill-resistant characteristics. Magic Eye - for determination of electrolyte level and state-of-charge. Easy to use - comes factory-charged and in ready-to-use condition. Robust design - to take care of stringent application requirements. Double clad separation - high reliability and life expectancy.', 'honda'),
(9, 'EXIDE INVA TUBULAR IT500-42 MONTHS WARRANTY', 'Tough battery with thicker plates for tough conditions.', 'http://www.batterybhai.com/common/uploaded_files/battery_image/4B71064F7E_1501303677_it500.jpg', 2050, 'EXIDE', 'S', 2, 'Tough battery with thicker plates for tough conditions.  Made with Exide\'s Torr Tubular technology. Housed in tall DIN containers.  More electrolyte - higher acid volume per ampere hour.  Most suitable for deep cycle application. Electrolyte level indicator.', 'TVS'),
(10, 'EXIDE FXL0-12XL5L-B', 'Unique Flame Arrestor ensures greater safety.', 'http://www.batterybhai.com/common/uploaded_files/battery_image/21585ECD86_1514269309_12xl5l-b_1.jpg', 1389, 'Exide', '2', 60, 'The revolutionary Gas Recombination system eliminates water loss making the battery completely maintenance-free.\r\nSpill-proof design guards against the possibility of leakage, even if mounted in a tilted or inclined position.\r\nAdvanced Lead-Calcium technology leads to low self-discharge.\r\nUnique Flame Arrestor ensures greater safety.\r\nAGM construction gives superior cranking power and better resistance to vibration and hence makes this battery the most reliable in the market today.\r\nThis permanently sealed VRLA battery comes factory-charged.\r\nIt needs no refilling, electrolyte or water.', 'honda'),
(11, 'EXIDE 3 KW OFF GRID SOLAR SYSTEM', 'Exide off grid solutions offer you independence to generate solar power', 'https://docs.exideindustries.com/images/products/solar-batteries/6lms-150l-batt.jpg', 315875, 'Exide', 'S', 9, 'Inverter/UPS - 3kw (01 Unit).\r\nBattery - 150 Ah (04 Unit).\r\nPanels - 3 KWP (270*12 Units).', 'TVS'),
(12, 'EXIDE INSTA BRITE IB1350', 'asy maintenance: Float/float guide to indicate electrolyte level', 'http://www.batterybhai.com/common/uploaded_files/battery_image/57F52C7862_1507017561_ib1350.jpg', 6699, 'Exide', 'I', 9, 'Unique features and superior technology: Advanced Hybrid Technology that is best suited to withstand high temperatures as well as thick plate construction with special paste formulation. Special hybrid alloy system leading to low water loss and dual plate separation(PE+GM) that reduces the possibility of premature failure.\r\nEasy handling/spill-proof: Moulded handles to ensure easy handling.Top vented lid with anti-splash guards fitted with coin flush vent plugs.\r\nReady for commissioning: Batteries supplied in factory-charged condition.\r\nReady for commissioning: Batteries supplied in factory-charged condition.', 'TVS'),
(13, 'EXIDE INSTA BRITE IB1500', 'Key Feature Battery Capacity: 150 Ah ', 'http://www.batterybhai.com/common/uploaded_files/battery_image/2A5A5C8379_1554019007_ib1500.jpg', 7699, 'Exide', 'I', 5, 'Battery Type: Flat Plate Battery Warranty: 36 Months(18 Months Free Of Cost + 18 Months Pro Rata).nique features and superior technology: Advanced Hybrid Technology that is best suited to withstand high temperatures as well as thick plate construction with special paste formulation. Special hybrid alloy system leading to low water loss and dual plate separation(PE+GM) that reduces the possibility of premature failure.Easy maintenance: Float/float guide to indicate electrolyte level.Fume and leak resistant: Spark arrestor fitted in float to restrict fumes and acid during operation. Clean top with no surface leakage.\r\n', 'TVS'),
(14, 'EXIDE INVA TUBULAR IT500', 'Tough battery with thicker plates for tough conditions. ', 'http://www.batterybhai.com/common/uploaded_files/battery_image/4B71064F7E_1501303677_it500.jpg', 12599, 'Exide', 'I', 10, 'Key Feature\r\nBattery Capacity: 150 Ah Battery Type: Tall Tubular Battery Warranty: 42 Months Free Of Cost.More electrolyte - higher acid volume per ampere hour. \r\nMost suitable for deep cycle application.\r\nElectrolyte level indicator.', 'honda'),
(15, 'Power Pack -FPC0-PC1000L', 'he rugged power packed range starts from cars, SUVs, trucks, tractors, construction vehicles, two wheelers and extends to batteries for a-rickshaws, gen-sets, inverters and solar applications. ', 'https://www.sfsonicpower.com/sites/default/files/PC1000L-Batt_0.jpg', 9140, 'SFSonic', 'I', 10, 'Lead Antimony (Pb-Sb) for Positive and Lead Calcium (Pb-Co) for Negative Plates -+ Deliver excellent power performance even in high ambient temperatures.\r\nRobust grid construction with PE Envelope and Glassmet Separators -+ Prevents active material from shedding which results in longer life.\r\nHigh charge retention and shelf life even when battery is not in use', 'honda'),
(16, 'Exide Solar UPS 1100va ,150ah Battery, 2 150wp Solar Panel', 'Capacity (Ah)	150AH', 'https://5.imimg.com/data5/KN/PA/MY-8766742/exide-solar-ups-1100va-2c150ah-battery-2c-2-150wp-solar-panel-500x500.jpg', 31999, 'Exide', 'S', 5, 'Warranty for ups - 24 months warranty for battery - 5 years free replacement warranty for solar panel - 10 + 15 =25 years', 'honda'),
(17, 'Exide 12V 7 Ah Powersafe Battery(Sealed),Original Replacement To Ups Battery', 'Exide 12V 7', 'https://images-na.ssl-images-amazon.com/images/I/31TelgQLAqL.jpg', 800, 'Exide', 'U', 6, 'Exide Ups Battery - Vrla - Dry Sealed Maintainance Free Battery.\r\nEco Friendly. Ready To Use - Factory Charged.\r\nGood Service Life.\r\nTechnology : Manufactured In Techincal Collaboration With Shin Kobe Electric Machinery Co Japan, Maker Of World Renowed Hitachi Batteries, Exide Industries Ltd Is An Iso 9001 Organisation.\r\nReliable performance.', 'honda'),
(18, 'EXIDE FXL0-XLTZ7', 'Unique Flame Arrestor ensures greater safety.', 'http://www.batterybhai.com/common/uploaded_files/battery_image/3147334FE9_1484397105_xltz7.jpg', 1760, 'Exide', '2', 15, 'The revolutionary Gas Recombination system eliminates water loss making the battery completely maintenance-free.\r\nSpill-proof design guards against the possibility of leakage, even if mounted in a tilted or inclined position.\r\nAdvanced Lead-Calcium technology leads to low self-discharge.\r\nUnique Flame Arrestor ensures greater safety.\r\nAGM construction gives superior cranking power and better resistance to vibration and hence makes this battery the most reliable in the market today.\r\nThis permanently sealed VRLA battery comes factory-charged.\r\nIt needs no refilling, electrolyte or water.', 'honda'),
(19, 'EXIDE MAGIC 625', 'Controlled Output Voltage', 'http://www.batterybhai.com/common/uploaded_files/battery_image/7F764F6B5A_1539060064_magic_625.jpg', 2799, 'Exide', 'I', 15, 'Wide Mains Input Voltage Range 100V - 290V.\r\nControlled Output Voltage.\r\nSuitable for Mains Low Voltage Operation, 8 - 10 AMP Charging at 120V Mains Input.\r\nASIC (Auto Sense Intelligent Control) technology for battery charging which automatically sense battery condition & adjust the charging current accordingly.\r\nSoft Touch ON/OFF, Mode Selection & Charging Selection Switch.\r\nSmart In-built Protections - Overload, Short Circuit, AC Back Feed, Battery Low, Battery over Charge, Mains Overload through Resettable Switch, Over Temperature etc.', 'honda');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `srno` int(11) NOT NULL,
  `batteryid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `address` varchar(500) NOT NULL,
  `pincode` int(6) NOT NULL,
  `ordertime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` int(12) NOT NULL,
  `orderstatus` varchar(50) NOT NULL DEFAULT 'Processing'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`srno`, `batteryid`, `userid`, `fullname`, `address`, `pincode`, `ordertime`, `amount`, `orderstatus`) VALUES
(16, 2, 8, 'Pranav Gurav', 'Jadhav Nagar,\r\nWadgaon Bk, Sinhgad Road', 411041, '2019-04-15 14:52:32', 0, 'Processing'),
(17, 1, 8, 'Pranav Gurav', 'Jadhav Nagar,\r\nWadgaon Bk, Sinhgad Road', 411041, '2019-04-15 14:59:48', 0, 'Completed'),
(18, 1, 8, 'Chaitanya Joshi', '113, Swami Krupa, Jadhav Nagar,\r\nWadgaon Bk, Sinhgad Road', 411041, '2019-04-16 10:22:04', 1800, 'Processing'),
(19, 11, 8, 'Vicky Jangam', 'Jadhav Nagar,\r\nWadgaon Bk, Sinhgad Road', 411041, '2019-04-16 11:18:06', 315875, 'Dispatched'),
(20, 16, 9, 'akshay more', 'bibewewadi,pune 51', 411053, '2019-04-16 12:03:04', 31999, 'Processing'),
(21, 12, 9, 'akshay more', 'solapuur 41', 52000, '2019-04-16 12:04:02', 6699, 'Processing'),
(22, 7, 9, 'pankaj pasalkar', 'Ghansoli Sector 1, New Mumbai\r\n\r\nNAVI MUMBAI', 400701, '2019-04-16 12:04:21', 2200, 'Processing'),
(23, 1, 9, 'Pratik Paigude', 'Jadhav Nagar,\r\nWadgaon Bk, Sinhgad Road', 411041, '2019-04-16 12:31:33', 1800, 'Processing'),
(24, 2, 10, 'Rahul Dua', '113, Swami Krupa, Jadhav Nagar,\r\nWadgaon Bk, Sinhgad Road', 411041, '2019-04-17 08:36:35', 2100, 'Processing');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`srno`);

--
-- Indexes for table `batteries`
--
ALTER TABLE `batteries`
  ADD PRIMARY KEY (`srno`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`srno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `srno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `batteries`
--
ALTER TABLE `batteries`
  MODIFY `srno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `srno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
