<?php
include_once 'inc/dbconnect.php';
include_once 'inc/secondary_functions.php';

$err = "";
if(is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}

if($_SERVER['REQUEST_METHOD']=="POST")
{
    if(isset($_POST['email']) && isset($_POST['pass']))
    {
        $db = new DbConnect();
        $email = $_POST['email'];
        $pass = $_POST['pass'];

        $sql = "select * from accounts where username='$email' and password = '$pass'";
        $quer = mysqli_query($db->getDb(),$sql);

        if(mysqli_num_rows($quer)>0)
        {
            $row = mysqli_fetch_row($quer);
            session_start();

            $_SESSION['username']= $row[1];
            $_SESSION['usersrno']= $row[0];
            $_SESSION['userrole']= $row[5];

            header("Location: http://$_SERVER[SERVER_NAME]");
        }
        else
        {
            $err .= " <h4 class=\"text-center bg-danger p-2 \"><span>Invalid ID/Password. Please try again.</span></h4>";
        }

    }

    mysqli_close($db->getDb());
}

?>


<!DOCTYPE HTML>
<html>
<head>
    <title>Login - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body>
<?php include 'inc/header.php'; ?>

<div class="container">
    <div class="row border p-4">
        <div class="col-lg-12">
            <div class="row">
                <?php echo $err; ?>
                <div class="col-lg-12">
                    <h2 class="display-2 text-center">Login</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4">
                    <form method="post">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" name="email"
                                   aria-describedby="emailHelp"
                                   placeholder="Enter email">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.
                            </small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" name="pass"
                                   placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="col-lg-4"></div>
            </div>
        </div>
    </div>
</div>
</body>

</html>