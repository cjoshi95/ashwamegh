<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/

$db = new DbConnect();


?>


<!DOCTYPE HTML>
<html>
<head>
    <title>FAQ - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
        src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body>
<?php include 'inc/header.php'; ?>
<div class="container">
    <div class="jumbotron">
        <h1 class="display-4">Welcome to Knowledge Base</h1>
        <p class="lead">Frequently Asked Questions</p>
        <hr class="my-4">

    </div>
    <div class="accordion" id="accordionExample">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        How do I know my car battery is failing?
                    </button>
                </h2>
            </div>

            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    There are few signs, which tell you that your car battery is getting old and need a replacement. Some useful signs that reflect the low or failing car battery are:<br><br>

                    • Inappropriate functioning of various electrical devices of the car<br><br>

                    • Problem in the ignition/starting<br><br>

                    • Your headlights look dim at idle and then brighten when you rev the engine<br><br>

                    • Improper cooling in the car cabin.<br><br>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        What are the causes of weak car battery?
                    </button>
                </h2>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                    There are several causes, which can cause weakening of car battery. If you know these and maintain your car battery, it can improve the battery life. Things that cause battery weakening or failure are like:<br><br>

                    • Massive use of battery power for lightning, air-conditioning, music system can make your battery weak<br><br>

                    • Rash driving<br><br>

                    • Low charge and acid stratification<br><br>

                    • Using air-conditioner, lights and music system, when car is in idle condition<br><br>

                    • Factory/manufacturing defects.<br><br>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingThree">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        How can I maintain my car battery?
                    </button>
                </h2>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body">
                    Proper maintenance of your car battery can increase it’s life and performance. Just follow some simple but vey useful tips to maintain your car batteries are:<br><br>

                    a. Clean the dust from your car battery on regular intervals, because it can cause corrosion<br><br>

                    b. If you find any kind of corrosion in your car battery, use solution of baking soda to wipe it out<br><br>

                    c. Clean the battery surface or case on regular intervals<br><br>

                    d. Periodically inspect the battery for several types of damages like fluid leaks, shell cracks, etc<br><br>

                    e. Keep you car battery always fully charged<br><br>

                    f.  Keep battery connectors like the wire clean<br><br>

                    g. Keep terminals and cables tight to prevent entry of toxins in the battery and also ensure proper flow of electricity<br><br>

                    h. To lubricate cables of the battery use Vaseline and petroleum jelly on regular intervals<br><br>

                    i. Switch off music system, air-conditioner and lights, when it is in idle condition<br><br>

                    j. Check the fluid level of the car battery on regular intervals to ensure full fluid level<br><br>

                    However, we recommend that you take proper caution while following any of the above tips as battery is an acidic and electrical device which may cause injuries if not handled properly.
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="headingFour">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                        Why is it important to remove the ground wire first while replacing the battery or cleaning the terminals?
                    </button>
                </h2>
            </div>

            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                <div class="card-body">
                    Before you start replacing the battery or cleaning the terminal, your first job is to check the type of grounding system your car has. If your car has negative ground system and you remove the positive connector first, then it may result in a spark. Spark may occur if you use metal tool to remove the positive terminal connector and it touches any metal part of the car. It may also result in battery explosion. Therefore it is very vital that you remove the ground wire first.
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="headingFive">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                        How excessive heat affects battery?
                    </button>
                </h2>
            </div>

            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                <div class="card-body">
                    Excessive heat evaporates water from the electrolyte, which results in oxidizing and weakening of the positive grid of the battery. Thus, excessive heat reduces the life of battery.
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="headingSix">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                        My car doesn’t start. Is it because the battery needs replacement?
                    </button>
                </h2>
            </div>

            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                <div class="card-body">
                    In addition to the battery, there may be several other reasons behind this. The best way to know that your car needs new battery is to have your battery tested. You can also judge if your battery needs replacement from some symptoms like a clicking noise coming from the car when you try to turn on the ignition, vehicle not starting at all and/or a sluggish turn over.
                </div>
            </div>
        </div>
    </div>

    <br><br>
    <h1><span class="display-4 mb-2">HOW TO JUMP START A CAR? <a href="kbase.php">Click here.</a></span></h1>

</div>
</body>
</html>