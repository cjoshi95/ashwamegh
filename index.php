<!DOCTYPE HTML>
<html>
<head>
    <title>Home - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body>
<?php include_once 'inc/header.php' ?>

<div class="container">
    <div class="jumbotron">
        <h1><i class="fas fa-car-battery"></i> Ashwamegh Batteries</h1>
        <p>Ashwmegh batteries is an Online Portal to get batteries of your choice.</p>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-6 mb-1 p-2">
            <div class="card" style="width: 18rem;">
                <img src="https://image.flaticon.com/icons/png/512/55/55283.png" class="card-img-top cover" alt="Car">
                <div class="card-body">
                    <h5 class="card-title">Car Battery</h5>
                    
                    <a href="explore.php?type=4" class="btn btn-info btn-md">Explore Now</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-1 p-2">
            <div class="card" style="width: 18rem;">
                <img src="https://cdn1.iconfinder.com/data/icons/transportation-28/100/7_Motorcycle-512.png"
                     alt="" class="card-img-top cover">
                <div class="card-body">
                    <h5 class="card-title">Two Wheeler Battery</h5>

                    <a href="explore.php?type=2" class="btn btn-info btn-md">Explore Now</a>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 mb-1 p-2">
            <div class="card" style="width: 18rem;">
                <img src="https://d30y9cdsu7xlg0.cloudfront.net/png/993-200.png"
                     alt="" class="card-img-top cover">
                <div class="card-body">
                    <h5 class="card-title">Three Wheeler Battery</h5>

                    <a href="explore.php?type=3" class="btn btn-info btn-md">Explore Now</a>
                </div>
            </div>
        </div>
          <div class="col-lg-4 col-md-6 mb-1 p-2">
            <div class="card" style="width: 18rem;">
                <img src="https://cdn1.iconfinder.com/data/icons/solar-powercons-service-series/512/solar-power-inverter-repair-error-512.png"
                     alt="" class="card-img-top cover">
                <div class="card-body">
                    <h5 class="card-title">Inverter Battery</h5>

                    <a href="explore.php?type=I" class="btn btn-info btn-md">Explore Now</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-1 p-2">
            <div class="card" style="width: 18rem;">
                <img src="https://cdn2.iconfinder.com/data/icons/home-appliances-51/32/battery-power-energy-car-ups-electric-512.png"
                     alt="" class="card-img-top cover">
                <div class="card-body">
                    <h5 class="card-title">UPS Battery</h5>

                    <a href="explore.php?type=U" class="btn btn-info btn-md">Explore Now</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 mb-1 p-2">
            <div class="card" style="width: 18rem;">
                <img src="https://cdn.iconscout.com/icon/premium/png-256-thumb/solar-panel-26-566936.png"
                     alt="" class="card-img-top cover">
                <div class="card-body">
                    <h5 class="card-title">Solar Panel's Battery</h5>

                    <a href="explore.php?type=S" class="btn btn-info btn-md">Explore Now</a>
                </div>
            </div>
        </div>

    </div>
</div>
</body>

</html>