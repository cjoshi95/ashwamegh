<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/

$db = new DbConnect();

?>

<!DOCTYPE HTML>
<html>
<head>
    <title>Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body>
<?php include 'inc/header.php'; ?>

<div class="container mt-5">
   <div class="row">
       <div class="col-md-6">
               <legend>Contact Us</legend>
               <div class="form-group">
                   <label for="Name" class="col-sm-2 control-label">Name</label>
                   <div class="col-sm-10">
                       <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                           <input type="text" id="Name" name="Name" class="form-control" placeholder="* Enter Name"
                                  data-bv-field="Name">
                       </div>
                       <small class="help-block" data-bv-validator="notEmpty" data-bv-for="Name" data-bv-result="NOT_VALIDATED"
                              style="display: none;">Your name is required
                       </small>
                   </div>
               </div>

               <div class="form-group">
                   <label for="Email" class="col-sm-2 control-label">Email</label>
                   <div class="col-sm-10">
                       <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                           <input type="email" id="Email" name="Email" class="form-control" placeholder="* Enter Email"
                                  data-bv-field="Email">
                       </div>
                       <small class="help-block" data-bv-validator="notEmpty" data-bv-for="Email"
                              data-bv-result="NOT_VALIDATED"
                              style="display: none;">Your email is required
                       </small>
                       <small class="help-block" data-bv-validator="emailAddress" data-bv-for="Email"
                              data-bv-result="NOT_VALIDATED" style="display: none;">Your email is not valid
                       </small>
                   </div>
               </div>

               <div class="form-group">
                   <label for="Phone" class="col-sm-2 control-label">Phone</label>
                   <div class="col-sm-10">
                       <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                           <input type="text" id="Phone" name="Phone" class="form-control" placeholder="Enter Phone Number">
                       </div>
                   </div>
               </div>

               <div class="form-group">
                   <label for="Message" class="col-sm-2 control-label">Message</label>
                   <div class="col-sm-10">
                       <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-comment fa-fw"></i></span>
                           <textarea id="Message" name="Message" class="form-control" rows="5"
                                     data-bv-field="Message"></textarea>
                       </div>
                       <small class="help-block" data-bv-validator="notEmpty" data-bv-for="Message"
                              data-bv-result="NOT_VALIDATED"
                              style="display: none;">Your message is required
                       </small>
                   </div>
               </div>

               <div class="form-group">
                   <div class="col-sm-2"></div>
                   <div class="col-sm-10" align="center">
                       <input type="checkbox" name="Consent" value="given" data-bv-field="Consent"> I agree to be contacted in
                       response to my message
                       <small class="help-block" data-bv-validator="choice" data-bv-for="Consent"
                              data-bv-result="NOT_VALIDATED"
                              style="display: none;">You need to tick the box
                       </small>
                   </div>
               </div>

               <div class="form-group">
                   <div class="col-sm-2"></div>
                   <div class="col-sm-10" align="center">
                       <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                   </div>
               </div>
       </div>
       <div class="col-md-6">
           <div class="row">
               <div class="col-md-4">
                   <div class="card" style="width: 18rem;">
                       <div class="card-body">
                           <h5 class="card-title">Customer Care Number</h5>
                           <p class="card-text"><i class="fas fa-phone"></i> +91 87 939393 71</p>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       </div>

   </div>
</div>

</body>
</html>