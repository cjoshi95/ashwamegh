<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/

$db = new DbConnect();


?>


<!DOCTYPE HTML>
<html>
<head>
    <title>Buy Battery - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body>
<?php include 'inc/header.php'; ?>

<div class="container mt-5">


    <?php

    if ($_SERVER['REQUEST_METHOD'] == "GET")
    {
    if (isset($_GET['product']))
    {
        $product = $_GET['product'];
        $sql = "select * from batteries where srno=" . $product;
        $quer = mysqli_query($db->getDb(), $sql);
        $row = mysqli_fetch_row($quer);
        ?>

        <div class="row">
        <div class="col-md-4">
        <div class="auto border border-primary d-flex justify-content-around">
            <img src="<?php echo $row[3]; ?>" class="buynow-logo">
        </div>

        <div class="mt-5">
            <span class="price">  Rs. <?php echo $row[4]; ?></span>
        </div>
        <div class="mt-2">
        <?php if ($row[7] == 0)
        {
            ?>
            <button class="btn btn-success btn-lg btn-block" disabled>Out of Stock</button>
            <?php
        } else
        {
            ?>
            <a class="btn btn-success btn-lg btn-block" href="<?php echo "http://$_SERVER[SERVER_NAME]/buy.php?product=$row[0]"; ?>" name="buynow" id="<?php echo $product; ?>">BUY NOW</a>
            <?php
        } ?>
        </div>

        </div>
        <div class="col-md-8">
            <h1 class="display-4 text-right"><?php echo $row[1]; ?></h1>
            <h5 class="text-right"
                style="font-weight:200;"><?php echo ($row[7] == 0) ? "Out of Stock" : $row[7] . " available in stock"; ?></h5>

            <p>
                <?php
                $desc = explode(".", $row[8]);
                array_pop($desc);
                echo "<ul>";
                foreach ($desc as $des)
                {
                    echo "<li>" . $des . "</li>";
                }
                echo "</ul>";
                ?>
            </p>

        </div>
        </div>

        <?php
    }
    }
    ?>
</div>

</body>
</html>