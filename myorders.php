<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/

$db = new DbConnect();


?>


<!DOCTYPE HTML>
<html>
<head>
    <title>My Orders - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


</head>
<body>
<?php include 'inc/header.php'; ?>

<div class="container mt-5">

    <div class="jumbotron">
        <h1 class="display-4">My Orders</h1>
        <i class="fas fa-shopping-cart"></i>

        <p class="lead">All the previous orders made by you are displayed here.</p>
        <hr class="my-4">
    </div>

    <?php
        $sql = "select * from orders where userid='$_SESSION[usersrno]'";
        $quer = mysqli_query($db->getDb(),$sql);

        while($row = mysqli_fetch_row($quer))
        {
            $batdata = "select * from batteries where srno = $row[1]";
            $batquer = mysqli_query($db->getDb(),$batdata);
            $batrow = mysqli_fetch_row($batquer);

            ?>

            <div class="row m-2 p-5 border border-success">
                <div class="col md-12">
                    <p class="text-left desc"><b>Order Status : </b><?php echo $row[8]; ?></p>
                    <p class="text-right"><b>Order ID : </b><?php echo $row[0]; ?></p>
                    <div class="row">
                        <div class="col-md-2">
                            <img src="<?php echo $batrow[3]; ?>"
                                 alt="" class="thumbnail iconsize">
                            <span class="desc"><b><?php echo $batrow[1]; ?></b></span>
                        </div>
                        <div class="col-md-10">
                            <h2 class="text-right"><?php echo $row[3]; ?></h2>
                            <h3 class="text-right desc"><b>Order Date : </b><?php echo $row[6]; ?></h3>
                            <h3 class="text-right desc"><b>Address : </b><?php echo $row[4]; ?></h3>
                            <h3 class="text-right desc"><b>Order Amount : </b><?php echo $row[7]; ?></h3>
                            <h3 class="text-right desc"><b>Payment Method : </b>Cash on Delivery</h3>
                            <button class="btn btn-danger">Cancel/Return Order</button>
                        </div>
                    </div>
                </div>
            </div>
    <?php
        }
    ?>






</div>

</body>
</html>
