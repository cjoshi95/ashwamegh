<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/
$err = "";
$db = new DbConnect();

?>

<!DOCTYPE HTML>
<html>
<head>
    <title>Battery Finder - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
        src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body>
<?php include 'inc/header.php'; ?>

<div class="container mt-5">
    <?php echo $err; ?>
    <form method="post" class="form-inline">
        <label for="brand">Select Battery Brand &nbsp;&nbsp;</label>
        <select name="brand" id="brand" class="form-control">
            <option value="--">-- SELECT A BRAND --</option>
            <option value="Exide">Exide</option>
            <option value="SFSonic">SF Sonic</option>
        </select>
        &nbsp;&nbsp;&nbsp;
        <label for="type">Select Battery Type &nbsp;&nbsp;</label>
        <select name="type" id="type" class="form-control">
            <option value="--">-- SELECT A TYPE --</option>
            <option value="2">Two Wheeler</option>
            <option value="3">Three Wheeler</option>
            <option value="4">Four Wheeler</option>
        </select>
        &nbsp;&nbsp;&nbsp;
        <label for="vehiclebrand">Vehicle Brand         &nbsp;&nbsp;</label>
        <select name="vehiclebrand" id="vehiclebrand" class="form-control">
            <option value="--">-- SELECT A VEHICLE BRAND --</option>
            <option value="honda">Honda</option>
            <option value="TVS">TVS</option>
            <option value="hero">Hero</option>
        </select>

        <button type="submit" class="btn btn-primary">Search</button>
    </form>

    <?php
    if($_SERVER['REQUEST_METHOD']=="POST")
    {
        if(isset($_POST['brand']) && isset($_POST['type']) && isset($_POST['vehiclebrand']))
        {
            $brand = $_POST['brand'];
            $type = $_POST['type'];
            $vehicle = $_POST['vehiclebrand'];

            if($brand=="--" || $type == "--" || $vehicle == "--")
            {
                $err .= "<span class='err'>Please select all options</span>";
            }
            else
            {
                $sql = "select * from batteries where type='$type' and brand='$brand' and vehiclebrand='$vehicle'";
                $quer = mysqli_query($db->getDb(),$sql);

                if(mysqli_num_rows($quer)>0)
                {
                    while ($row = mysqli_fetch_row($quer))
                    {
                        echo "<div class=\"col-lg-4 col-md-6 mb-1\">
            <div class=\"border m-2 p-2\">
                <div class=\"auto\">";

                        echo "<img src='" . $row[3] . "' alt='icon' class=\"thumbnail iconsize\"></div><div>
                <h4 class='card-title'>" . $row[1] . "</h4><ul><li>";

                        echo $row[2] . "</li></ul>";

                        echo "<b>Battery Brand</b> : " . $row[5] . "<br>";
                        echo "<b>Vehicle Type</b> : " . nameFromType($row[6]) . "<br>";
                        echo "<b>Vehicle Brand</b> : " . ucfirst($row[9]) . "<br>";
                        echo "<b>Availability</b> : "; echo ($row[7] == 0) ? "Out of Stock" : $row[7] . " available in stock";

                        echo "<br><br><a href='http://$_SERVER[SERVER_NAME]/buynow.php?product=$row[0]' class='btn btn-success btn-md buynow' id='" . $row[0] . "'> Buy Now </a></div></div></div>";
                    }
                }
                else
                {
                    echo "<span>0 items found for your query</span>";
                }

            }
        }
    }
    ?>
</div>
</body>
</html>