<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/

$db = new DbConnect();


?>


<!DOCTYPE HTML>
<html>
<head>
    <title>Order Data - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
          crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
          crossorigin="anonymous">


</head>
<body>
<?php include 'inc/header.php'; ?>



<div class="container mt-5">

    <?php

    if($_SERVER['REQUEST_METHOD']=="GET")
    {
        if(isset($_GET['type']))
        {
            $type = $_GET['type'];
            echo "<h1 class='display-4'>". nameFromType($type)." Batteries</h1>";
            echo "<div class=\"row\">";
            $sql = "select * from batteries where type='$type'";
            $quer = mysqli_query($db->getDb(),$sql);

            while($row = mysqli_fetch_row($quer))
            {
                echo "<div class=\"col-lg-4 col-md-6 mb-1\">
            <div class=\"border m-2 p-2\">
                <div class=\"auto\">";

                echo "<img src='" . $row[3] . "' alt='icon' class=\"thumbnail iconsize\"></div><div>
                <h4 class='card-title'>" . $row[1] . "</h4><ul><li>";

                echo $row[2] . "</li></ul>";
                echo "<p><b>Cost : </b>Rs . $row[4]</p>";
                echo "<button class='btn btn-success btn-md buynow' id='" . $row[0] . "'> Buy Now </button></div></div></div>";
            }

            echo "  </div>";

        }
    }

    ?>



</div>
</body>
</html>

