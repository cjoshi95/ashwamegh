<?php
include 'inc/secondary_functions.php';
include_once 'inc/dbconnect.php';
/*
if(!is_loggedIn())
{
    header("Location: http://$_SERVER[SERVER_NAME]");
}*/

$db = new DbConnect();

?>

<!DOCTYPE HTML>
<html>
<head>
    <title>Store - Ashwamegh Enterprises</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script
            src="http://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

</head>
<body>
<?php include 'inc/header.php'; ?>

<div class="container">
    <h1 class="display-3 text-center">Welcome to Our Store</h1>
    <div class="row">

        <?php
        $sql = "select * from batteries";
        $query = mysqli_query($db->getDb(), $sql);

        while ($row = mysqli_fetch_row($query))
        {
            echo "<div class=\"col-lg-4 col-md-6 mb-1\">
            <div class=\"border m-2 p-2\">
                <div class=\"auto\">";

            echo "<img src='" . $row[3] . "' alt='icon' class=\"thumbnail iconsize\"></div><div>
                <h4 class='card-title'>" . $row[1] . "</h4><ul><li>";

            echo $row[2] . "</li></ul>";
            echo "<p><b>Cost : </b>Rs . $row[4]</p>";
            echo "<button class='btn btn-success btn-md buynow' id='" . $row[0] . "'> Buy Now </button></div></div></div>";
        }

        ?>


        <!--        <div class="col-lg-4 col-lg-4 col-md-6 mb-1">-->
        <!--            <div class="border m-2 p-2">-->
        <!--                <div class="auto">-->
        <!--                    <img src="http://www.batterybhai.com/common/uploaded_files/battery_image/438007377E_1512648750_mk-1440-tz4.jpg"-->
        <!--                         class="thumbnail iconsize" alt="SF_Sonic">-->
        <!--                </div>-->
        <!--                <div>-->
        <!--                    <h4 class="card-title">SF SONIC MK1440-TZ4</h4>-->
        <!--                    <ul>-->
        <!--                        <li>The revolutionary Gas Recombination system eliminates water loss making the battery-->
        <!--                            completely maintenance free.-->
        <!--                        </li>-->
        <!--                        <li>The Spill Proof design guards against the possibility of leakage, even if mounted in a-->
        <!--                            tilted or inclined position.-->
        <!--                        </li>-->
        <!--                        <li>The advanced Lead-Calcium technology leads to low self-discharge.</li>-->
        <!--                        <li>Unique Flame Arrestor ensures greater safety.</li>-->
        <!--                        <li>The AGM construction gives superior cranking power and better resistance to vibration and-->
        <!--                            hence makes this battery the most reliable in the market today.-->
        <!--                        </li>-->
        <!--                        <li>The permanently sealed VRLA battery is factory-charged does not need re-filling, electrolyte-->
        <!--                            or water.-->
        <!--                        </li>-->
        <!--                    </ul>-->
        <!--                    <button class="btn btn-success btn-md">Buy Now</button>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->


    </div>
</div>


<script type="application/javascript" src="js/scripts.js"></script>
</body>

</html>