<?php

function is_loggedIn()
{
    if(isset($_SESSION['username']))
        return true;
    else
        return false;
}

function nameFromType($var)
{
    switch($var)
    {
        case "2":
            return "Two Wheeler";
            break;
        case "3":
            return "Three Wheeler";
            break;
        case "4":
            return "Four Wheeler";
            break;
        case "I":
            return "Inverter";
            break;
        case "S":
            return "Solar Panels";
            break;
        case "U":
            return "UPS";
            break;
        default:
            return "--";
    }
}

