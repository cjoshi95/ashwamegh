<?php include_once 'secondary_functions.php';
session_start();
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


<nav class="navbar fixed-top  navbar-expand-lg navbar-dark bg-dark mb-5">
    <a class="navbar-brand" href="#"><img src="img/logo.png" class="logosize" alt="Logo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="store.php">Online Store</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="finder.php">Battery Finder</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact.php">Contact Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="about.php">About Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="faq.php">FAQ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="locate.php"><i class="fas fa-map-marker-alt"></i> Locate Us</a>
            </li>

        </ul>


        <?php
        if (is_loggedIn())
        {

            ?>
            <ul class="navbar-nav my-2 my-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo $_SESSION['username'] ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="myorders.php">My Orders</a>
                        <div class="dropdown-divider"></div>
                        <?php
                        if ($_SESSION['userrole'] == "Dis")
                        {
                            echo "<a class=\"dropdown-item\" href=\"dispanel.php\">Distributor Panel</a>";
                        } else if ($_SESSION['userrole'] == "Adm")
                        {
                            echo "<a class=\"dropdown-item\" href=\"admpanel.php\">Admin Panel</a>";
                        }
                        ?>
                    </div>
                </li>
                <li class="nav-item"><a class="nav-link" href="logout.php">Logout</a></li>
            </ul>
            <?php
        } else
        {
            ?>
            <ul class="navbar-nav my-2 my-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="register.php">Signup</a>
                </li>

            </ul>
            <?php
        }
        ?>
    </div>
</nav>